[constants]
arch = 'aarch64'
toolchain_target = arch + '-linux-android28'

[built-in options]
cpp_args = ['-target', toolchain_target]
cpp_link_args = ['-target', toolchain_target, '-static-libstdc++']

[host_machine]
system = 'android'
cpu_family = arch
cpu = arch
endian = 'little'
