cros-trace
----------

cros-trace consists of

 - `cros-trace` python script to start tracing on DUT host/guest or locally
 - `cros-trace-tool` to be copied to or installed on DUT host/guest or locally
 - [perfetto](https://perfetto.dev/) SDK as a meson subproject
 - a stripped-down version of
   [vperfetto](https://github.com/741g/vperfetto.git) to merge DUT host/guest
   traces

## Dependencies

`cros-trace-tool` depends on protobuf.  It also needs to be cross-compiled for
DUT hosts and guests.  On distros derived from Debian, you can

    $ apt install meson ccache g++-aarch64-linux-gnu g++-arm-linux-gnueabihf \
                  protobuf-compiler libprotobuf-dev
    $ apt install adb google-android-ndk-r25-installer

to get the dependencies.

## Build and Usage

The easiest way to build cros-trace is to

    $ ./build-all out

This builds `cros-trace-tool` for the host machine, cross-compiles it for
supported DUT hosts and guests, and packages everything.

To trace `$DUT` and its Android guest,

    $ cd out/cros-trace
    $ ./cros-trace -d $DUT -a

This assumes `ssh $DUT` and `adb shell` have been set up to work.

To trace `$DUT` and its Linux guest,

    $ ./cros-trace -d $DUT -G $VM_NAME

You'll need to manually start `traced_probes` as root and `traced` as chronos
(or whoever the default vsh user is) inside the VM first.

Alternatively, to run inside the Crostini container,

    $ ./cros-trace -d $DUT -g $DUT_GUEST

Again, this assumes `ssh $DUT_GUEST` has been set up to work (using `ProxyJump`
ssh config for example).

`cros-trace` uses `configs/perfetto.cfg` as the config by default.  This can
be overridden by `-c`.

To see all options,

    $ ./cros-trace -h

## More Usage

`cros-trace` can be used to trace only the DUT host or only the DUT guest,
rather than both

    $ ./cros-trace -d $DUT
    $ ./cros-trace -g $DUT_GUEST
    $ ./cros-trace -G $DUT:$VM_NAME
    $ ./cros-trace -a
    $ ./cros-trace -l

This usage is provided for convenience.  It is usually more flexible to trace
manually, such as

    $ cat configs/perfetto.cfg | ssh $DUT 'perfetto --txt -c - -o /tmp/perfetto.trace'
    $ scp -C $DUT:/tmp/perfetto.trace .

perfetto's own `record_android_trace` is recommended when tracing just the
Android guest

    $ wget https://raw.githubusercontent.com/google/perfetto/master/tools/record_android_trace
    $ chmod u+x record_android_trace
    $ ./record_android_trace -c configs/perfetto.cfg --sideload -n -o perfetto.trace
      (or use subprojects/perfetto/tools/record_android_trace directly)

Note that some of the options in `configs/perfetto.cfg` might need to be
commented out because `perfetto` on Android might be too old to understand
them.

In fact, when tracing both the DUT host and guest, `cros-trace` automates a
few things

 - read TSC offset of the guest vm
 - copy `cros-trace-tool` to both the DUT host and guest
 - start tracing on both the DUT host and guest
 - copy traces back
 - run `cros-trace-tool merge` to merge the traces

It is possible to run `cros-trace-tool` directly and skip `cros-trace` script
completely.

## Misc Tips

### SSH

`cros-trace` expects SSH connection sharing.  An example to set up SSH
connection sharing would be

    Host <dut>
        User root
        IdentityFile <path-to-testing_rsa>
        UserKnownHostsFile /dev/null
        StrictHostKeyChecking no
        ControlMaster auto
        ControlPath ~/.ssh/control-%r@%h:%p
        ControlPersist 10

If `$DUT` runs on a test image, you can use the testing private key at
`test-keys/cros-testing_rsa`.

### ADB

ADB can use vendor keys to skip manual authentication

    $ ADB_VENDOR_KEYS=<path-to-key> adb connect $DUT

The vendor key for the Android guest is at `test-keys/cros-arcvm_key`.

### GPU Counters and Render Stages

Some boards have perfetto-enabled host drivers.  On such boards, one can

    $ ssh $DUT pps-producer

to start the perfetto performance producer before tracing.

For boards that do not have a perfetto-enabled host drivers, one needs to add
`-Dperfetto=true` to the ebuild file of the host driver, rebuild, and deploy.

### Perfetto Uprev

In the rare cases where the perfetto subproject is updated, it is necessary to

    $ meson subprojects update

to fetch the latest revision.

### Development Flow

`cros-trace` accepts `--bin-dir` and `--data-dir`.  They can be convenient
when making changes to the script

    $ ./build-all out
    $ ./scripts/cros-trace -c configs/perfetto.cfg --bin-dir out/cros-trace ...
