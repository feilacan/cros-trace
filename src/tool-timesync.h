// Copyright 2022 Google LLC
// SPDX-License-Identifier: MIT

#pragma once

#include "tool-common.h"

#include <pthread.h>

struct timesync_thread {
    pthread_t thread;
    std::atomic_bool run;
};

#ifdef TOOL_ENABLE_TIMESYNC

int
timesync_thread_create(struct timesync_thread *thrd);

void
timesync_thread_join(struct timesync_thread *thrd);

int
tool_timesync_main(int argc, char **argv);

#else

static inline int
timesync_thread_create(struct timesync_thread *thrd)
{
    return 0;
}

static inline void
timesync_thread_join(struct timesync_thread *thrd)
{
}

static inline int
tool_timesync_main(int argc, char **argv)
{
    return tool_unsupported_main(argc, argv);
}

#endif
