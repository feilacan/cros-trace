// Copyright 2022 Google LLC
// SPDX-License-Identifier: MIT

#pragma once

#include "tool-common.h"

#ifdef TOOL_ENABLE_MERGE

int
tool_merge_main(int argc, char **argv);

#else

static inline int
tool_merge_main(int argc, char **argv)
{
    return tool_unsupported_main(argc, argv);
}

#endif
