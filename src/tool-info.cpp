// Copyright 2022 Google LLC
// SPDX-License-Identifier: MIT

#include "tool-info.h"

#include <fstream>
#include <inttypes.h>

#include "perfetto_trace.pb.h"

struct tool_info {
    const char *filename;
    perfetto::protos::Trace trace;
};

#define INFO_STAT(stats, name, spec)                                           \
    if ((stats).has_##name())                                                  \
    tool_info(#name ": %" spec, (stats).name())

static void
info_print_buffer_stats(
    const struct tool_info *info,
    const perfetto::protos::TraceStats_BufferStats &buffer_stats)
{
    INFO_STAT(buffer_stats, buffer_size, PRIu64);
    INFO_STAT(buffer_stats, bytes_written, PRIu64);
    INFO_STAT(buffer_stats, bytes_overwritten, PRIu64);
    INFO_STAT(buffer_stats, bytes_read, PRIu64);
    INFO_STAT(buffer_stats, padding_bytes_written, PRIu64);
    INFO_STAT(buffer_stats, padding_bytes_cleared, PRIu64);
    INFO_STAT(buffer_stats, chunks_written, PRIu64);
    INFO_STAT(buffer_stats, chunks_rewritten, PRIu64);
    INFO_STAT(buffer_stats, chunks_overwritten, PRIu64);
    INFO_STAT(buffer_stats, chunks_discarded, PRIu64);
    INFO_STAT(buffer_stats, chunks_read, PRIu64);
    INFO_STAT(buffer_stats, chunks_committed_out_of_order, PRIu64);
    INFO_STAT(buffer_stats, write_wrap_count, PRIu64);
    INFO_STAT(buffer_stats, patches_succeeded, PRIu64);
    INFO_STAT(buffer_stats, patches_failed, PRIu64);
    INFO_STAT(buffer_stats, readaheads_succeeded, PRIu64);
    INFO_STAT(buffer_stats, readaheads_failed, PRIu64);
    INFO_STAT(buffer_stats, abi_violations, PRIu64);
    INFO_STAT(buffer_stats, trace_writer_packet_loss, PRIu64);
}

static void
info_print_trace_stats(const struct tool_info *info,
                       const perfetto::protos::TraceStats &trace_stats)
{
    for (int i = 0; i < trace_stats.buffer_stats_size(); i++) {
        tool_info("buffer_stats: %d", i);
        info_print_buffer_stats(info, trace_stats.buffer_stats(i));
    }

    INFO_STAT(trace_stats, producers_connected, PRIu32);
    INFO_STAT(trace_stats, producers_seen, PRIu64);
    INFO_STAT(trace_stats, data_sources_registered, PRIu32);
    INFO_STAT(trace_stats, data_sources_seen, PRIu64);
    INFO_STAT(trace_stats, tracing_sessions, PRIu32);
    INFO_STAT(trace_stats, total_buffers, PRIu32);
    INFO_STAT(trace_stats, chunks_discarded, PRIu64);
    INFO_STAT(trace_stats, patches_discarded, PRIu64);
    INFO_STAT(trace_stats, invalid_packets, PRIu64);

    if (trace_stats.has_filter_stats()) {
        const auto &filter_stats = trace_stats.filter_stats();
        INFO_STAT(filter_stats, input_packets, PRIu64);
        INFO_STAT(filter_stats, input_bytes, PRIu64);
        INFO_STAT(filter_stats, output_bytes, PRIu64);
        INFO_STAT(filter_stats, errors, PRIu64);
    }

    INFO_STAT(trace_stats, flushes_requested, PRIu64);
    INFO_STAT(trace_stats, flushes_succeeded, PRIu64);
    INFO_STAT(trace_stats, flushes_failed, PRIu64);
    INFO_STAT(trace_stats, final_flush_outcome, PRIu32);
}

static void
info_print_trace(const struct tool_info *info)
{
    for (int i = 0; i < info->trace.packet_size(); i++) {
        const auto &packet = info->trace.packet(i);
        if (packet.has_trace_stats()) {
            info_print_trace_stats(info, packet.trace_stats());
            break;
        }
    }
}

static void
info_parse_trace(struct tool_info *info)
{
    std::ifstream is(info->filename);
    info->trace.ParseFromIstream(&is);
}

static void
info_usage(int exit_code)
{
    tool_info("Usage: " TOOL_COMM " info"
              " <TRACE>"
              " [--help]");
    exit(exit_code);
}

static void
info_parse_options(struct tool_info *info, int argc, char **argv)
{
    for (int i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "--help") || !strcmp(argv[i], "-h")) {
            info_usage(0);
        } else {
            if (info->filename)
                info_usage(1);
            info->filename = argv[i];
        }
    }

    if (!info->filename)
        info_usage(1);
}

int
tool_info_main(int argc, char **argv)
{
    struct tool_info info {};
    info_parse_options(&info, argc, argv);

    info_parse_trace(&info);
    info_print_trace(&info);

    return 0;
}
