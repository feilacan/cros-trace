#!/bin/sh

set -e

if [ $# -lt 1 ]; then
	echo "Usage: $0 <output-dir>"
	exit 1
fi

OUT="$(realpath "$1")"
: ${CCACHE:="$(which ccache)"}
: ${SYSTEMS:="linux android"}
: ${ABIS:="x86_64 aarch64 armv7a"}
: ${ANDROID_NDK_ROOT:="/usr/lib/android-ndk"}

# usage: use LIST_VAR WORD
# Test if WORD is in the space-separated LIST.
use() {
    eval list=\$$1

    for x in $list; do
        if [ "$x" = "$2" ]; then
            return 0
        fi
    done

    return 1
}

# Quiet `which`
which_q() {
    which "$@" >/dev/null
}

if use ABIS aarch64 && ! which_q aarch64-linux-gnu-g++; then
	echo "aarch64-linux-gnu-g++ is missing"
	exit 1
fi

if use ABIS armv7a && ! which_q arm-linux-gnueabihf-g++; then
	echo "arm-linux-gnueabihf-g++ is missing"
	exit 1
fi

if ! which protoc > /dev/null; then
	echo "protoc is missing"
	exit 1
fi

if ! pkg-config protobuf; then
	echo "protobuf is missing"
	exit 1
fi

if use SYSTEMS android && [ ! -d "$ANDROID_NDK_ROOT" ]; then
    echo "ANDROID_NDK_ROOT is not a directory"
    echo "ANDROID_NDK_ROOT=$ANDROID_NDK_ROOT"
    exit 1
fi

TARGETS=""
for sys in $SYSTEMS; do
	for abi in $ABIS; do
		TARGETS="$TARGETS $sys-$abi"
	done
done

setup_all() {
	options="--prefix $OUT/install --buildtype release --strip"

	meson setup "$OUT/out-host" $options -Dtool-timesync=false

	# cross-compile cros-trace-tool
	ndk_bin="$ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/linux-x86_64/bin"
	for target in $TARGETS; do
		CROSS_FILE="$target.txt"
		CXX=""
		STRIP=""
		cross_options="$options -Dscripts=false -Dtool-info=false -Dtool-merge=false"
		if [[ "$target" = android-* ]]; then
			CXX="$CCACHE $ndk_bin/clang++"
			STRIP="$ndk_bin/llvm-strip"
		elif [ -n "$CCACHE" ]; then
			CROSS_FILE="ccache-$CROSS_FILE"
		fi

		if [[ "$target" = *-armv7a ]]; then
			cross_options="$cross_options -Dtool-timesync=false"
		fi

		CXX="$CXX" STRIP="$STRIP" meson setup "$OUT/out-$target" \
			$cross_options --bindir "share/cros-trace/$target" \
			--cross-file "meson-cross/$CROSS_FILE"
	done
}

compile_all() {
	meson compile -C "$OUT/out-host"

	for target in $TARGETS; do
		meson compile -C "$OUT/out-$target"
	done
}


install_all() {
	meson install -C "$OUT/out-host" --skip-subprojects

	for target in $TARGETS; do
		meson install -C "$OUT/out-$target" --skip-subprojects
	done
}

package_all() {
	timestamp=$(date +%Y%m%d)
	git_sha=$(git rev-parse --short=8 HEAD)
	package="cros-trace-$timestamp-git-$git_sha"

	echo "Packaging $package.tar.gz"

	mkdir -p "$OUT/$package"
	cp -a "$OUT/install/bin/"* "$OUT/$package"
	cp -a "$OUT/install/share/cros-trace/"* "$OUT/$package"

	tar -zcf "$OUT/$package.tar.gz" -C "$OUT" "$package"
	ln -nsf "$package" "$OUT/cros-trace"
}

setup_all
compile_all
install_all
package_all
